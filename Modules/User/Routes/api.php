<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\User\Http\Controllers\API\V1\UserAPIController;
use Modules\User\Http\Controllers\API\V1\UserAuthAPIController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1/user')->group(function (){
    Route::post('create', [UserAPIController::class, 'create']);
    Route::post('login', [UserAuthAPIController::class, 'login']);

    Route::middleware('auth:api')->group(function (){

        Route::get('/', [UserAPIController::class, 'show']);
        Route::put('edit', [UserAPIController::class, 'update']);
        Route::delete('/', [UserAPIController::class, 'delete']);

        Route::get('logout', [UserAuthAPIController::class, 'logout']);
    });

});
