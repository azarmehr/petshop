<?php

namespace Modules\User\Http\Controllers\API\V1;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\User\Http\Requests\UserCreateAPIRequest;
use Modules\User\Http\Requests\UserDelteRequest;
use Modules\User\Http\Requests\UserUpdateRequest;
use Modules\User\Services\UserService;
use Modules\User\Transformers\NewRegisteredUserResource;
use Modules\User\Transformers\UserResource;

class UserAPIController extends Controller
{

    public function __construct(protected UserService $userService)
    {
    }

    /**
     * @OA\Get(
     *   path="/api/v1/user",
     *   summary="Get user",
     *   @OA\Response(
     *     response=200,
     *     description="User Account Details"
     *   ),
     *   security={{"token": {}}},
     * )
     */
    public function show()
    {
        return response()->customJson(true ,new UserResource(Auth::user()));
    }

    /**
     * @OA\Post(
     *     path="/api/v1/user/create",
     *     summary="Add a user",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="first_name",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="last_name",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="email",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="address",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password_confirmation",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="phone_number",
     *                     type="string"
     *                 ),
     *                 example={"first_name": "Azarmehr", "last_name": "Raghi", "email": "azarmehr@test.com",
     *                          "password": "password", "password_confirmation": "password", "address":"test",
     *                          "phone_number": "9300935080"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */

    public function create(UserCreateAPIRequest $request)
    {
        $validated = $request->validated();

        $user = $this->userService->registerNewUser($validated);

        return response()->customJson(true ,new NewRegisteredUserResource($user));
    }

    /**
     * @OA\Put(
     *     path="/api/v1/user/edit",
     *     summary="Update user",
     *     security={{"token": {}}},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="first_name",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="last_name",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="email",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="address",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password_confirmation",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="phone_number",
     *                     type="string"
     *                 ),
     *                 example={"first_name": "Azarmehr", "last_name": "Raghi", "email": "azarmehr@test.com",
     *                          "password": "password", "password_confirmation": "password", "address":"test",
     *                          "phone_number": "9300935080"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function update(UserUpdateRequest $request)
    {
        $validated = $request->validated();

        $user = $this->userService->updateUserData(Auth::user(), $validated);

        return response()->customJson(true ,new UserResource($user));
    }


    /**
     * @OA\Delete(
     *   path="/api/v1/user",
     *   summary="Delete user",
     *   security={{"token": {}}},
     *   @OA\Response(
     *     response=200,
     *     description="User Account Deleted"
     *   )
     * )
     */
    public function delete()
    {
        $this->userService->deleteUser(Auth::user());

        return response()->customJson();
    }
}
