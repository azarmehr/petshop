<?php

namespace Modules\User\Http\Controllers\API\V1;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\User\Http\Requests\UserLoginRequest;

class UserAuthAPIController extends Controller
{

    /**
     * @OA\Post(
     *     path="/api/v1/user/login",
     *     summary="Add a user",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="email",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     type="string"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    public function login(UserLoginRequest $request)
    {
        $credentials = $request->validated();

        if ($token = Auth::attempt($credentials)){
            return response()->customJson(true, [
                'token' => $token
            ]);
        }

        return response()->customJson(false, null, 'Invalid Credentials');
    }

    /**
     * @OA\Get(
     *   path="/api/v1/user/logout",
     *   summary="Log out user",
     *   security={{"token": {}}},
     *   @OA\Response(
     *     response=200,
     *     description="User Logged out"
     *   )
     * )
     *
     */
    public function logout()
    {
        Auth::logout();

        return response()->customJson();
    }
}
