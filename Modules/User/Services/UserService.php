<?php

namespace Modules\User\Services;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Modules\User\Entities\User;

class UserService
{

    public function registerNewUser(array $data)
    {
        $userData = $data;
        $userData['password'] = Hash::make($data['password']);
        $userData['uuid'] = Str::orderedUuid();

        $user = User::create($userData);
        return $user;
    }

    public function updateUserData($user, array $data)
    {
        $userData = $data;
        if (array_key_exists('password', $data)){
            $userData['password'] = Hash::make($data['password']);
        }
        $user->update($userData);

        return $user->fresh();
    }

    public function deleteUser($user)
    {
        $user->delete();
    }
}
