# PetShop

This Laravel application can be run using Docker Compose. Before starting the app with Docker Compose, you need to set the environment variables used by the Docker Compose file. You can use `env.example` for that purpose.

you can check the API documentation using the route: http://localhost:8080/api/documentation
## Setting up Environment Variables

1. Copy the `env.example` file to a new file named `.env`:
   ```bash
   cp .env.example .env
Open the .env file and configure the necessary environment variables for your application. Typically, you'll set variables like the database connection details and application-specific settings.

##Running the Application with Docker Compose
To start the Laravel application using Docker Compose, follow these steps:

1. Make sure you have Docker and Docker Compose installed on your system.

2. Navigate to the root directory of your Laravel application.

3. Build and start the Docker containers in the background:```docker-compose up -d```

4. After the containers are up and running, connect to the PHP container to execute commands using the following Docker command:
```docker exec -it pet-shop-app sh```

5. Once you are inside the PHP container, you can run Laravel-specific commands as needed.

```
composer install

RUN chown -R www-data:www-data /var/www/html/storage /var/www/html/bootstrap/cache

RUN chmod -R 755 /var/www/html/storage /var/www/html/bootstrap/cache

RUN chown -R www-data:www-data /var/www/html/public

php artisan key:generate

php artisan migrate
```

That's it! The application should now be up and running using Docker Compose.
