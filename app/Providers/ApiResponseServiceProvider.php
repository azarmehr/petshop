<?php

namespace App\Providers;

use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class ApiResponseServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot(ResponseFactory $factory): void
    {
        $factory->macro('customJson',function ($success = true,$data = null,$error = null, $errors = [], $extra = []){
            return Response::make([
                'success' => $success,
                'data' => $data,
                'error' => $error,
                'errors' => $errors,
                'extra' => $extra
            ]);
        });
    }
}
