<?php

namespace App\Http\Controllers;
/**
 * @OA\Info(title="My First API", version="0.1")
 */
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;


class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    /**
     * @OA\SecurityScheme(
     *   securityScheme="token",
     *   type="apiKey",
     *   name="Authorization",
     *   in="header"
     * )
     */

    public function test()
    {

    }
}
